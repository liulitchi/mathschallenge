"""
How many different triplets of positive integers add to make one-thousand?

If we have a < b < c for positive integers, and we already known that a + b + c = 1000.
then we can tell that:
1 <= a <= 332
a < b <= 499
b < c <= 997
"""
count = 0
for a in range (1, 333):
	for b in range (a + 1, 500):
		for c in range (b + 1, 998):
			if a + b + c == 1000:
				count += 1
print(count)
